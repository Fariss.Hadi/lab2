package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    private List<FridgeItem> fridge= new ArrayList<FridgeItem>();
    private int maxfridgesize=20;

    public int nItemsInFridge() {
        int fridgesize = fridge.size();
        return fridgesize;
    }

    public int totalSize() {
        return maxfridgesize;
    }


    public boolean placeIn(FridgeItem item) {
        if (fridge.size()<maxfridgesize){
            fridge.add(item);
            return true;
        }
        else{
        return false;
    }}

    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)){
            fridge.remove(item);
        }
        else{throw new NoSuchElementException();}

    }


    public void emptyFridge() {
        fridge.clear();

    }

    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired= new ArrayList<FridgeItem>();

        for ( FridgeItem item : fridge ){
            if (item.hasExpired()){
                expired.add(item);
            }
        }
        for ( FridgeItem item : expired ){
                takeOut(item);
        }

        return expired;
    }
}
